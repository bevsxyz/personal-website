---
# An instance of the Contact widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

#title: Contact
#subtitle: Let's connect!

content:
  # Automatically link email and phone or display as text?
  autolink: true
  
  # Email form provider
  form:
    provider: formspree
    formspree:
      id: mdopkkyq
      captcha: true
      captcha_key: 6Ldh15EbAAAAAN2PLbToUvshKX-YHtkl4xOL5OcP
    netlify:
      # Enable CAPTCHA challenge to reduce spam?
      captcha: false
  
design:
  columns: '0'
---
