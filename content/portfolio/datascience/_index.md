---
title: DataScience
---

Welcome to my datascience portfolio!

{{% callout note %}}
If you would like a quick overview of my professional life, head over to my [resume](/files/resume.pdf).
{{% /callout %}}
